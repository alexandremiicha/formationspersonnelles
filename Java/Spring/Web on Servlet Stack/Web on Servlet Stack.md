# Web on Servlet Stack
Lien : **https://docs.spring.io/spring-framework/docs/6.0.4/reference/html/web.html**

Lien avec reactive : **https://docs.spring.io/spring-framework/docs/6.0.4/reference/html/web-reactive.html**

## Spring Web MVC
### Contrôleur

```java
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HelloController {

    @GetMapping("/hello")
    public String handle(Model model) {
        model.addAttribute("message", "Hello World!");
        return "index";
    }
}
```
### Type de requête
Il existe également des variantes de raccourci spécifiques à la méthode HTTP @RequestMapping:
- @GetMapping
- @PostMapping
- @PutMapping
- @DeleteMapping
- @PatchMapping

### Consumes
Indique ce que le format des données qu'il accepte en entrée
```java
@PostMapping(path = "/pets", consumes = "application/json")
```

### Produces
Indique le format qui va être retourné par le controleur
```java
@GetMapping(path = "/pets/{petId}", produces = "application/json")
```

### Paramètres et en-tête
Test si mon param est égal à mon test
```java
@GetMapping(path = "/pets/{petId}", params = "myParam=myValue")
```

Test si mon header est égal à mon test
```java
@GetMapping(path = "/pets", headers = "myHeader=myValue")
```

### @PathVariable : Récupérer les paths d'une url 
```java
@GetMapping("/owners/{ownerId}/pets/{petId}")
public Pet findPet(@PathVariable Long ownerId, @PathVariable Long petId) {
    // ...
}
```

### @RequestParam : Récupérer les paramètres d'une url 
```java
@GetMapping
public String setupForm(@RequestParam("petId") int petId, Model model) 
```

### @RequestHeader : Récupérer les valeurs d'en-tête
```java
@GetMapping("/demo")
public void handle(
        @RequestHeader("Accept-Encoding") String encoding, 
        @RequestHeader("Keep-Alive") long keepAlive) 
```

### @CookieValue : Récupérer un cookie
```java
@GetMapping("/demo")
public void handle(@CookieValue("JSESSIONID") String cookie)
```

### @ModelAttribute : Passer dans la vue des objets
```java
@PutMapping("/accounts/{account}")
public String save(@ModelAttribute("account") Account account)
```

### @SessionAttributes : Récupérer la session 
```java
@Controller
@SessionAttributes("pet") 
public class EditPetForm {

    @PostMapping("/pets/{id}")
    public String handle(Pet pet, BindingResult errors, SessionStatus status) {
        if (errors.hasErrors) {
        }
        status.setComplete(); 
    }
}
```

### @SessionAttribute : Accéder aux attributs de la session
```java
@GetMapping("/")
public String handle(@RequestAttribute Client client) 
```

### @RequestBody : Récupérer le body de la requête
```java
@PostMapping("/accounts")
public void handle(@RequestBody Account account)
```

### ResponseEntity : Personnaliser la réponse de la requête
```java
@GetMapping("/something")
public ResponseEntity<String> handle() {
    String body = ... ;
    String etag = ... ;
    return ResponseEntity.ok().eTag(etag).body(body);
}
```

### @InitBinder initialisation d'instance
```java 
@InitBinder 
public void initBinder(WebDataBinder binder) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    dateFormat.setLenient(false);
    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
}
```

### @ExceptionHandler : Catcher les exceptions
```java
@ExceptionHandler
public ResponseEntity<String> handle(IOException ex) {
        // ...
        }
        
@ExceptionHandler({FileSystemException.class, RemoteException.class})
public ResponseEntity<String> handle(Exception ex) {
    // ...
}
```

### @ControllerAdvice : Gérer les erreurs des contrôleurs
```java
@ControllerAdvice(annotations = RestController.class)
public class ExampleAdvice1 {}

// Target all Controllers within specific packages
@ControllerAdvice("org.example.controllers")
public class ExampleAdvice2 {}

// Target all Controllers assignable to specific classes
@ControllerAdvice(assignableTypes = {ControllerInterface.class, AbstractController.class})
public class ExampleAdvice3 {}
```