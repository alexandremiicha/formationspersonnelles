# Utiliser de la donnée en BDD

## Package
```xml
<!-- JPA Data (We are going to use Repositories, Entities, Hibernate, etc...) -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<!-- Use MySQL Connector-J (choisir une version si besoin) -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

## Configuration 
```xml
spring.datasource.url=jdbc:mysql://localhost:3306/invest
spring.datasource.username=root
spring.datasource.password=
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5Dialect
spring.jpa.hibernate.ddl-auto=create-drop

## Show logs of sql request
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE


## Show logs of sql request
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE
```
La configuration de spring.jpa.hibernate.ddl-auto est importante, elle défini s'il y a besoin de supprimer et de réécrire complètement la base de donnée ou par exemple simplement rien faire si elle est déjà présente

## Annotation des tables
@Entity(name="") : créer une table à partir de cette classe
@Table : personnaliser les informations de la table (exemple : nom de table)
@Id : mettre une clef primaire
@GeneratedValue : générer la valeur de la clef primaire (personnalisable)
@Column : personnaliser les informations d'une colonne 
@Transient : rendre un champ non persistant
@Temporal : enregistrer des valeurs temporelles 
@Enumerated : persister un type énumération
@Basic : ajouter des informations sur une colonne niveau java et non bdd (éviter de parser des choses en bdd non possible)
