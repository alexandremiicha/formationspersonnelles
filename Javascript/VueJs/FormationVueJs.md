# VueJs Training

## Création projet  
```bash
npm init vue@latest
```

## Démarrage projet / build
```bash
npm run dev
npm run build
```

## vite.config.ts
Choisir le port du lancement du projet
```js
import { defineConfig } from 'vite'

export default defineConfig({
  server: {
    port: 8080
  }
})
```

## API styles
### Options API
```js
<script>
export default {
  // Properties returned from data() become reactive state
  // and will be exposed on `this`.
  data() {
    return {
      count: 0
    }
  },

  // Methods are functions that mutate state and trigger updates.
  // They can be bound as event listeners in templates.
  methods: {
    increment() {
      this.count++
    }
  },

  // Lifecycle hooks are called at different stages
  // of a component's lifecycle.
  // This function will be called when the component is mounted.
  mounted() {
    console.log(`The initial count is ${this.count}.`)
  }
}
</script>

<template>
  <button @click="increment">Count is: {{ count }}</button>
</template>
```

### Composition API
```js
<script setup>
import { ref, onMounted } from 'vue'

// reactive state
const count = ref(0)

// functions that mutate state and trigger updates
function increment() {
  count.value++
}

// lifecycle hooks
onMounted(() => {
  console.log(`The initial count is ${count.value}.`)
})
</script>

<template>
  <button @click="increment" >Count is: {{ count }}</button>
</template>
```

## Ref vs ComputedRef vs Reactive 
Ref permet uniquement de rendre une variable dynamique sur un seul type
```ts
const state = ref(0)
```

ComputedRef permet de rendre une variable dynamique à partir de valeur dynamique (Ref)
```ts
const state = computed( () => {return state+1} )
```

Reactive permet uniquement de rendre une variable dynamique sur un objet
```ts
const state = reactive({ count: 0 })
```

## Rendu Conditionnel

### v-if & v-else-if & v-else
```html
<div v-if="type === 'A'">
</div>
<div v-else-if="type === 'B'">
</div>
<div v-else-if="type === 'C'">
</div>
<div v-else>
</div>
```
### v-show
```html
<h1 v-show="ok">Hello!</h1>
```

### v-if vs v-show
La différence entre v-if et v-show est que v-show garde le contenu de son code dans le dom et affectue un display:none

## Rendu de liste
### v-for
```ts
const items = ref([{ message: 'Foo' }, { message: 'Bar' }])
<li v-for="item in items" :key="item.messsage">
  {{ item.message }}
</li>
```

## Gestion des évènements

### @click
@click applique la méthode lorsque l'on clique sur l'éléments en questions
```ts
const name = ref('Vue.js')

function greet(event) {
  alert(`Hello ${name.value}!`)
  // `event` is the native DOM event
  if (event) {
    alert(event.target.tagName)
  }
}

<button @click="greet">Greet</button>
```

## Gestion de formulaires
```html
<p>Message is: {{ message }}</p>
<input v-model="message" placeholder="edit me" />
```

