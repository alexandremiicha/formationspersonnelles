# Redux

## Terminologie :
- Action : Une action envoie une data de l'application à Redux store. Une action est conventionnellement un objet avec 2 paramètres un **type** et un **payload** (optionnel).

```js
{
  type: DELETE_TODO,
  payload: id,
}
```
- Reducer : un reducer est une fonction qui prend 2 paramètres : State & Action

```jsx
const initialState = {
  todos: [
    { id: 1, text: 'Eat' },
    { id: 2, text: 'Sleep' },
  ],
  loading: false,
  hasErrors: false,
}

function todoReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload),
      }
    default:
      return state
  }
}
```

- Store : L'état de l'application Redux vit dans le magasin, qui est initialisé avec un reducer.

```jsx
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './reducers'

const store = createStore(reducer)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
```

- Dispatch : dispatch une méthode disponible sur l'objet de magasin qui accepte un objet utilisé pour mettre à jour l'état Redux.

```jsx
const Component = ({ dispatch }) => {
  useEffect(() => {
    dispatch(deleteTodo())
  }, [dispatch])
}
```

- Connect : connect est une fonction qui permet de connecter React à Redux 

## Mise en place
